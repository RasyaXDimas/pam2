package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.view.View
import android.widget.TextView
import com.example.myapplication.R.id.editTextTextPersonName

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val nama = intent.getStringExtra("name")
        val gender = intent.getStringExtra("kelamin")
        val program = intent.getStringExtra("prodi")

        val genders = findViewById<TextView>(R.id.textView5)
        val name = findViewById<TextView>(R.id.textView4)
        val prodi = findViewById<TextView>(R.id.textView9)

        name.text = "Nama\t\t: $nama"
        genders.text="Gender\t: $gender"
        prodi.text="$program"


    }
}